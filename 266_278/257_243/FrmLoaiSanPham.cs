﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _266_278
{
    public partial class FrmLoaiSanPham : Form
    {
        public FrmLoaiSanPham()
        {
            InitializeComponent();
        }
        clSQLbanhang con = new clSQLbanhang();
        DataSet dsLoaiSP = new DataSet();
        private void FrmLoaiSanPham_Load(object sender, EventArgs e)
        {
            LoadDL_dgv(dgv, "Select * from LOAISP");
            LoadDL_textBox(0);
            xulychucnang(true);
            
        }
        void LoadDL_dgv(DataGridView d, string sql)
        {
            dsLoaiSP = con.laydulieu(sql);
            d.DataSource = dsLoaiSP.Tables[0];
        }
        void LoadDL_textBox(int vitri)
        {
            txtmaloai.Text = dsLoaiSP.Tables[0].Rows[vitri]["MALOAI"].ToString();
            txttenloai.Text = dsLoaiSP.Tables[0].Rows[vitri]["TENLOAI"].ToString();
            string l=dsLoaiSP.Tables[0].Rows[vitri][2].ToString();
            if (l == "Hoạt động")
                cbo.SelectedIndex = 1;
            else
                cbo.SelectedIndex = 0;

            
        }
        void xulychucnang(Boolean t)
        {
            btnthem.Enabled = t;
            btnsua.Enabled = t;
            btnxoa.Enabled = t;
            btnhuy.Enabled = !t;
            btnluu.Enabled = !t;
        }
        private void txtmaloai_TextChanged(object sender, EventArgs e)
        {
            
        }
        int flag = 0;
        private void dgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int vitri = dgv.CurrentCell.RowIndex;
            LoadDL_textBox(vitri);
        }
        //vo hiệu hoá nút x
        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        } 
        string phatsinhma(DataSet ds)
        {
            string madt = ds.Tables[0].Rows[ds.Tables[0].Rows.Count - 1][0].ToString();
            int d = int.Parse(madt.Substring(0, madt.Length - 0));
            madt =(d + 1).ToString();
            return madt;
        }
        private void cbo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnthem_Click(object sender, EventArgs e)
        {
            xulychucnang(false);
            txtmaloai.Text = phatsinhma(dsLoaiSP);
            flag = 1;
        }

        private void btnsua_Click(object sender, EventArgs e)
        {
            xulychucnang(false);
            flag = 2;
            LoadDL_dgv(dgv, "Select * from LOAISP ");
            txtmaloai.ReadOnly = true;
            
        }

        private void btnxoa_Click(object sender, EventArgs e)
        {
            xulychucnang(false);
            flag = 3;
        }

        private void btnthoat_Click(object sender, EventArgs e)
        {
            if(flag!=0)
            {
                DialogResult dlg;
                dlg= MessageBox.Show("Bạn có muốn lưu trước khi thoát ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlg == DialogResult.Yes)
                {
                    string sql = "insert into LoaiSP values('" + txtmaloai.Text + "','";
                    sql += txttenloai.Text + "','" + cbo.Text + "')";
                    if (con.capnhatdulieu(sql) != 0)
                    {
                        MessageBox.Show("Cập nhật thành công");
                        LoadDL_dgv(dgv, "Select * from LOAISP");
                        this.Close();
                    }
                }
                else
                    this.Close();
            }
            else
            {
                this.Close();
            }
        }

        private void btnhuy_Click(object sender, EventArgs e)
        {
            xulychucnang(true);
            txttenloai.Text = "";
            txtmaloai.Text = "";
            flag = 0;
        }

        private void btnluu_Click(object sender, EventArgs e)
        {
            xulychucnang(true);
            string sql = "";
            if (flag==1)
            {
                sql = "insert into LoaiSP values('" + txtmaloai.Text + "','";
                sql += txttenloai.Text + "',N'" + cbo.Text+ "')";
                
            }
            if(flag==2)
            {
                LoadDL_dgv(dgv, "Select * from LOAISP ");
                sql = "update LOAISP set TENLOAI='" + txttenloai.Text + "',TRANGTHAI=N'" + cbo.Text + "'where MALOAI='" + txtmaloai.Text + "'";
                
            }
            if(flag==3)
            {
                sql = "update LOAISP set TRANGTHAI =N'Ngưng hoạt động' where MALOAI='" + txtmaloai.Text + "'";
            }
            if (con.capnhatdulieu(sql) != 0)
            {
                MessageBox.Show("Cập nhật thành công");
                LoadDL_dgv(dgv, "Select * from LOAISP");
                xulychucnang(true);
                flag = 0;
            }
        }
    }
}
