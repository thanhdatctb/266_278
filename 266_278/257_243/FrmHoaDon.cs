﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _266_278
{
    public partial class FrmHoaDon : Form
    {
        public FrmHoaDon()
        {
            InitializeComponent();
        }
        clSQLbanhang con = new clSQLbanhang();
        DataSet dsHD = new DataSet();
        DataSet dsNV = new DataSet();
        void LoadDanhSach_DatagridView()
        {
            dsHD = con.laydulieu("select * from HOADON");
            dgv.DataSource = dsHD.Tables[0];
        }
        //vo hiệu hoá nút x
        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        } 
        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        string phatsinhMAHD()
        {
            string mahd="";
            DataSet dsHD=con.laydulieu("select mahd from hoadon");
            if(dsHD.Tables[0].Rows.Count<=0)
                mahd="hd001";
            else
                mahd="hd00"+ (dsHD.Tables[0].Rows.Count + 1).ToString();
            return mahd;

        }
        int flag = 0;
        private void button3_Click(object sender, EventArgs e)
        {
            textBox1.Text= phatsinhMAHD();
            dgv1.Rows.Clear();
            flag = 1;

        }

        private void btnthoat_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FrmHoaDon_Load(object sender, EventArgs e)
        {
            LoadDanhSach_DatagridView();
            dsNV = con.laydulieu("select * from NHANVIEN");
            LoadDL_ComboBox(dsNV,cboNhanVien , "tennv", "manv");
        }

        private void LoadDL_ComboBox(DataSet dsNV)
        {
            throw new NotImplementedException();
        }

        private void label12_Click(object sender, EventArgs e)
        {

        }
        clSQLbanhang c = new clSQLbanhang();
        string makh = "";

        void LoadDL_ComboBox(DataSet ds, ComboBox c, string tennv, string manv)
        {
            c.DataSource = ds.Tables[0];
            c.DisplayMember = tennv;
            c.ValueMember = manv;
            c.SelectedIndex = -1;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            string sql = "select makh,tenkh,SDT from khachhang where SDT='" + textBox2.Text + "'";
            DataSet dsKH = new DataSet();
            dsKH = c.laydulieu(sql);
            if (dsKH.Tables[0].Rows.Count > 0)
            {
                makh = dsKH.Tables[0].Rows[0][0].ToString();
                textBox4.Text = dsKH.Tables[0].Rows[0][1].ToString();
                textBox3.Text = dsKH.Tables[0].Rows[0][0].ToString();
            }
            else
            {
                MessageBox.Show("Sdt Không Tồn Tại");
                FrmKhachHang frm = new FrmKhachHang();
                frm.Show();
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void cboNhanVien_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox14_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode==Keys.Enter)
            {
                DataSet dsSP = con.laydulieu("select * from sanpham where masp" + "=" + "'" + txtmasp.Text + "'");
                txttensp.Text = dsSP.Tables[0].Rows[0]["tensp"].ToString();
                txtgiaban.Text = dsSP.Tables[0].Rows[0]["giaban"].ToString();
                string h = dsSP.Tables[0].Rows[0]["hinh"].ToString();
                string filename = "C:\\Users\\My PC\\Desktop\\257_243\\hinh\\";
                    filename += h;
                    Bitmap a = new Bitmap(filename);
                    pictureBox1.Image = a;
                    pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;

            }
        }

        private void textBox14_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
        float tongtien = 0;
        private void button2_Click_1(object sender, EventArgs e)
        {
            float tt = 0;
            tt = int.Parse(txtgiaban.Text) * int.Parse(txtsl.Text) * int.Parse(txtkm.Text);
            tongtien += tt;
            txttt.Text =tongtien.ToString();

            object []t={txtmasp.Text, txtgiaban.Text, txtsl.Text, txtkm.Text, tt.ToString() };
            dgv.Rows.Add(t);
        }

     


        private void btnluu_Click(object sender, EventArgs e)
        {

            
        }
    }
}
