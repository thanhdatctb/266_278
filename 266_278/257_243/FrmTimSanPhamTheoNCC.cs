﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _266_278
{
    public partial class FrmTimSanPhamTheoNCC : Form
    {
        public FrmTimSanPhamTheoNCC()
        {
            InitializeComponent();
        }
        clSQLbanhang con = new clSQLbanhang();
        private void FrmTimSanPhamTheoNCC_Load(object sender, EventArgs e)
        {
            DataSet dsNCC = con.laydulieu("select MANCC,TENCC from NHACUNGCAP");
            cbo.DataSource = dsNCC.Tables[0];
            cbo.DisplayMember = "TENCC";
            cbo.ValueMember = "MANCC";
            cbo.SelectedIndex = -1;
        }

        private void cbo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbo.SelectedIndex != -1)
            {
                string mancc = cbo.SelectedValue.ToString();
                DataSet ds = new DataSet();
                dgv.DataSource = con.laydulieu("select * from SANPHAM where MANCC='" + mancc + "'").Tables[0];
            }
        }
    }
}
