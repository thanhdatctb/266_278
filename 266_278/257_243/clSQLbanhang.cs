﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace _266_278
{
    class clSQLbanhang
    {
        SqlConnection con = new SqlConnection();
        private object p;
        void ketnoi()
        {
            con.ConnectionString = @"data source =.\SQLEXPRESS; 
                    initial catalog=quanlybanhang; integrated security=true";
            if (con.State == ConnectionState.Closed)
                con.Open();
        }
        public clSQLbanhang()
        {
            ketnoi();
        }

        public DataSet laydulieu(string sql)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(sql, con);
            da.Fill(ds);
            return ds;
        }
        public int capnhatdulieu(string sql)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = sql;
            cmd.CommandType=CommandType.Text;
            cmd.Connection = con;
            return cmd.ExecuteNonQuery();
        }
    }
}
