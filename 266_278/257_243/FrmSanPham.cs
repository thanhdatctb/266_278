﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _266_278
{
    public partial class FrmSanPham : Form
    {
       
        public FrmSanPham()
        {
            
            InitializeComponent();
        }
        clSQLbanhang con= new clSQLbanhang();
        DataSet dsSP = new DataSet();
        DataSet dsNCC = new DataSet();
        DataSet dsLoaiSP = new DataSet();
        void LoadDanhSach_DatagridView(DataGridView d, string sql)
        {
            dsSP = con.laydulieu(sql);
            d.DataSource = dsSP.Tables[0];
        }
        //vo hiệu hoá nút x
        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        } 
        void LoadDL_ComboBox(DataSet ds,ComboBox c,string ten,string ma)
        {
            c.DataSource = ds.Tables[0];
            c.DisplayMember = ten;
            c.ValueMember = ma;
            c.SelectedIndex = -1;
        }
        void LoadDL_textBox(int vitri)
        {
            txtmasp.Text = dsSP.Tables[0].Rows[vitri]["MASP"].ToString();
            txttensp.Text = dsSP.Tables[0].Rows[vitri]["TENSP"].ToString();
            txtchitiet.Text = dsSP.Tables[0].Rows[vitri]["CHITIET"].ToString();
            txtgianhap.Text = dsSP.Tables[0].Rows[vitri]["GIANHAP"].ToString();
            txtgiaban.Text = dsSP.Tables[0].Rows[vitri]["GIABAN"].ToString();
            txthinh.Text = dsSP.Tables[0].Rows[vitri]["HINH"].ToString();
            txtmau.Text = dsSP.Tables[0].Rows[vitri]["MAUSAC"].ToString();
            txtsoluong.Text = dsSP.Tables[0].Rows[vitri]["SOLUONG"].ToString();
            string mancc, maloai;
            mancc = dsSP.Tables[0].Rows[vitri]["MANCC"].ToString();
            maloai = dsSP.Tables[0].Rows[vitri]["MALOAI"].ToString();
            //loc du lieu len combobox
            DataView dvNCC = new DataView();
            dvNCC.Table = dsNCC.Tables[0];
            cbo1.DataSource = dvNCC;
            cbo1.DisplayMember = "TENCC";
            cbo1.ValueMember = "MANCC";

            dvNCC.RowFilter = "MANCC='" + mancc + "'";
            //loc du lieu len combobox
            DataView dvLOAISP = new DataView();
            dvLOAISP.Table = dsLoaiSP.Tables[0];
            cbo2.DataSource = dvLOAISP;
            cbo2.DisplayMember = "TENLOAI";
            cbo2.ValueMember = "MALOAI";
            dvLOAISP.RowFilter = "MALOAI='" + maloai + "'";
            string l = dsSP.Tables[0].Rows[vitri][10].ToString();
            if (l == "Hoạt động")
                cbo3.SelectedIndex = 1;
            else
                cbo3.SelectedIndex = 0;
        }
        private void btnthoat_Click(object sender, EventArgs e)
        {
            if (flag != 0)
            {
                DialogResult dlg;
                dlg = MessageBox.Show("Bạn có muốn lưu trước khi thoát ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlg == DialogResult.Yes)
                {
                    string sql = "insert into SANPHAM values('" + txtmasp.Text + "',N'";
                    sql += txttensp.Text + "',N'" + txtchitiet.Text + "','" + txtgianhap.Text + "','" + txtgiaban.Text + "','" + txthinh.Text + "',N'" + txtmau.Text + "','" + txtsoluong.Text + "','" + cbo1.Text + "','" + cbo2.Text + "')";
                    if (con.capnhatdulieu(sql) != 0)
                    {
                        MessageBox.Show("Cập nhật thành công");
                        LoadDanhSach_DatagridView(dgv, "Select * from SANPHAM");
                        this.Close();
                    }
                }
                else
                    this.Close();
            }
            else
            {
                this.Close();
            }
        }
        
        private void FrmSanPham_Load(object sender, EventArgs e)
        {
            LoadDanhSach_DatagridView(dgv, "Select * from SANPHAM");
            dsNCC = con.laydulieu("select * from NHACUNGCAP");
            dsLoaiSP = con.laydulieu("select * from LOAISP");
            //hien len combo
            LoadDL_ComboBox(dsNCC, cbo1, "TENCC","MANCC");
            LoadDL_ComboBox(dsLoaiSP, cbo2, "TENLOAI", "MALOAI");
            xulychucnang(true);
            LoadDL_textBox(0);
            
        }
        private void btnthem_Click(object sender, EventArgs e)
        {
            flag = 1;
            xulychucnang(false);
            txtmasp.Text = phatsinhma(dsSP);
            //loaddata lên cbo1
            DataSet dsNCC = con.laydulieu("select MANCC,TENCC from NHACUNGCAP");
            cbo1.DataSource = dsNCC.Tables[0];
            cbo1.DisplayMember = "TENCC";
            cbo1.ValueMember = "MANCC";
            cbo1.SelectedIndex = -1;


            //loaddata lên cbo2
            DataSet dsLoaiSP = con.laydulieu("select MALOAI,TENLOAI from LOAISP");
            cbo2.DataSource = dsLoaiSP.Tables[0];
            cbo2.DisplayMember = "TENLOAI";
            cbo2.ValueMember = "MALOAI";
            cbo2.SelectedIndex = -1;


            
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog o = new OpenFileDialog();
            o.ShowDialog();
            if (o.FileName != "")
            {
                Bitmap a = new Bitmap(o.FileName);
                pictureBox1.Image = a;
                pictureBox1.SizeMode=PictureBoxSizeMode.StretchImage;
            }
        }

        private void btnhuy_Click(object sender, EventArgs e)
        {
            xulychucnang(true);
            txttensp.Text = "";
            txtchitiet.Text = "";
            txtgianhap.Text = "";
            txtgiaban.Text = "";
            txthinh.Text = "";
            txtmau.Text = "";
            txtsoluong.Text = "";
            flag = 0;
            
        }
        void xulychucnang(Boolean t)
        {
            btnthem.Enabled = t;
            btnsua.Enabled = t;
            btnxoa.Enabled = t;
            btnhuy.Enabled = !t;
            btnluu.Enabled = !t;
        }

        private void btnsua_Click(object sender, EventArgs e)
        {
            flag = 2;
            txtmasp.ReadOnly = true;
            cbo1.Enabled = false;
            cbo2.Enabled = false;
            
            xulychucnang(false);
            //loaddata lên cbo1
            DataSet dsNCC = con.laydulieu("select MANCC,TENCC from NHACUNGCAP");
            cbo1.DataSource = dsNCC.Tables[0];
            cbo1.DisplayMember = "TENCC";
            cbo1.ValueMember = "MANCC";
           


            //loaddata lên cbo2
            DataSet dsLoaiSP = con.laydulieu("select MALOAI,TENLOAI from LOAISP");
            cbo2.DataSource = dsLoaiSP.Tables[0];
            cbo2.DisplayMember = "TENLOAI";
            cbo2.ValueMember = "MALOAI";
            

        }

        private void btnxoa_Click(object sender, EventArgs e)
        {
            xulychucnang(false);
            flag = 3;
            
            //loaddata lên cbo1
            DataSet dsNCC = con.laydulieu("select MANCC,TENCC from NHACUNGCAP");
            cbo1.DataSource = dsNCC.Tables[0];
            cbo1.DisplayMember = "TENCC";
            cbo1.ValueMember = "MANCC";
            cbo1.SelectedIndex = -1;


            //loaddata lên cbo2
            DataSet dsLoaiSP = con.laydulieu("select MALOAI,TENLOAI from LOAISP");
            cbo2.DataSource = dsLoaiSP.Tables[0];
            cbo2.DisplayMember = "TENLOAI";
            cbo2.ValueMember = "MALOAI";
            cbo2.SelectedIndex = -1;
        }

        private void dgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int vitri = dgv.CurrentCell.RowIndex;
            LoadDL_textBox(vitri);
        }
        string phatsinhma(DataSet ds)
        {
            string madt = ds.Tables[0].Rows[ds.Tables[0].Rows.Count - 1][0].ToString();
            int d = int.Parse(madt.Substring(0, madt.Length - 0));
            madt = (d + 1).ToString();
            return madt;
        }

        private void cbo1_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }
        int flag = 0;
        private void btnluu_Click(object sender, EventArgs e)
        {
            xulychucnang(true);
            string sql = "";
            if (flag == 1)
            {
                sql = "insert into SANPHAM values('" + txtmasp.Text + "',N'";
                sql += txttensp.Text + "',N'" + txtchitiet.Text + "','" + txtgianhap.Text + "','" + txtgiaban.Text + "','" + txthinh.Text + "',N'" + txtmau.Text + "','" + txtsoluong.Text + "','" + cbo1.SelectedValue +"','" + cbo2.SelectedValue + "',N'" + cbo3.Text + "')";

            }
            
            if (flag == 2)
            {
                LoadDanhSach_DatagridView(dgv, "Select * from SANPHAM");
                sql = "update SANPHAM set TENSP=N'" + txttensp.Text + "',CHITIET=N'" + txtchitiet.Text + "',GIANHAP='" + txtgianhap.Text + "',GIABAN=N'" + txtgiaban.Text + "',HINH='" + txthinh.Text + "',MAUSAC=N'" + txtmau.Text + "',SOLUONG='" + txtsoluong.Text + "',TRANGTHAI=N'" + cbo3.Text + "' where MASP='" + txtmasp.Text + "'";

            }
            if (flag == 3)
            {
                sql = "update SANPHAM set TRANGTHAI =N'Ngưng hoạt động' where MASP='" + txtmasp.Text + "'";
            }
             
            if (con.capnhatdulieu(sql) != 0)
            {
                MessageBox.Show("Cập nhật thành công");
                LoadDanhSach_DatagridView(dgv, "Select * from SANPHAM");
                xulychucnang(true);
                flag = 0;
            }
        }

        private void FrmSanPham_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
       
    }
}
